# Donations
Maintaining this wiki incurs costs associated with the following:
* Running the server which hosts the wiki
* Domain renewal fees

I no longer play Clash of Clans, but I am happy to continue hosting this wiki. Please do not donate if you are a student, or are not financially comfortable in doing so.

---
If you would like to help contribute to the upkeep of this wiki, you can show your support [here](https://ko-fi.com/pkyle).