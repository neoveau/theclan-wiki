Clan Rules
==========

*   Promotion is earned
*   Clean chat: There are kids playing in this clan, too
*   Respect others: We are civilised
*   Be Active: We chat quite a bit, but please don't spam
*   DO NOT rush your base. Being a war clan, [war weight](/pages/war-weight) is important.

Please type the phrase “_Great rules_" in the chat when you return so that we know you were here. Thanks!

Outside of War
--------------

*   Donate before requesting, and Donate regularly
*   No max troop requests unless for war attacks

During War
----------

*   War search every Friday and Tuesday evening 8.30pm ([Australia Eastern Standard time, GMT+1000hrs](http://www.timeanddate.com/worldclock/australia/sydney))
*   NO LOOTING: We fight our clan wars together as one clan.
*   No claiming bases. [Follow the strategy](/strategy).
*   Only allow max level donations for war attacks
*   Do NOT donate to war castles without Leaders' permission: We usually discuss a strategy of filling war castles before each war (See the [war preparation](/pages/war-preparation) section for more information)
*   Protect your Town Hall in your War Base: If the enemy cannot destroy your Town Hall, the maximum number of stars is only one.
*   Come back here (this wiki: [https://theclan.wiki](https://theclan.wiki)) during Preparation Day of each war to see your recommended first enemy base to attack
*   Attack your assigned base early so that others can clean up if you didn't 3 star your base
*   You MUST battle twice each war: If you cannot battle for some reason, drop out from this clan and join the “feeder” clan:
    
    Search for “@ the clan 1” where the Leader is Dan.
    
    Princess Xena describes it thus: “It is a holiday/vacation/I-need-a-break clan.” Feel free to re-join the main clan when you are ready again.
    
    Note also: “You must come online in the holiday clan at least once per week otherwise you will be kicked if leaders think you are no longer active.”
    
*   If you want to be effective in war, you need to understand [war weight](/pages/war-weight). This will explain why poorly rushed bases are so bad for war.