# War Preparation
During Preparation Day, those with stronger troops will donate for war castles. Please do not donate unless agreed with Leaders.

The current strategy is to vary castle troops to throw the enemy off-guard.

## About War Bases
Protect your Town Hall, preferably place it at the centre of your structures. If the enemy cannot destroy your Town Hall, the most they'll get is one star. Leave your storage (Gold, Elixir and Dark Elixir) and related buildings (Gold Mine, Elixir Collector and Dark Elixir Drill ) outside the walls. In War, their utility is simply like walls, slow down the enemy troops for your defensive buildings (Cannon, Archer Tower, Mortar and Wizard Tower) to do their stuff. Some warriors like to leave a building at each corner to waste time for the attacking troops.