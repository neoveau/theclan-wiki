> If you want to do well in war, it is important that you understand war weight.

# What is war weight?
Your **war weight** is a calculation of your base's total defensive and offensive capability, and it is the primary influence to the matchmaking system in Clan Wars. The matchmaking system will try to match one clan with another clan having *similar strength*, or *war weight*. Overall, **clan weight** is the combined weight of each base. Each base has it’s own war weight number that represents the base strength.

When people upgrade to Town Hall 9, they are often quick to build their Xbows. But why do that? Xbows weigh a lot, and if your walls are still level 8 (or even lower), a bad match could occur in Clan Wars. This is not Supercell’s fault; the war search is for two clans with the same combined war weight. Therefore, it is ideal to maintain a lower war weight to gain an advantage in the clan war matchmaking.

## Important Notes
- Your *war weight* will increase more from stronger defences (like Xbows and Inferno Towers). 
- The higher your war weight gets, the stronger opponents you and the clan will get.
- Maintaining a lower war weight provides you and the clan with an advantage in matchmaking.

As a member of @ The Clan, your offence should always be prioritised before defensive upgrades. Please refer to the [upgrade priority guide](/pages/upgrade-priority) for a detailed list of the most effective buildings to upgrade for each town hall.

# Other Resources
- 🛡️ [ClashTrack.com](https://clashtrack.com) - *War Weight Calculator*