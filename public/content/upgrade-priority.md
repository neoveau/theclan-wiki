# Base Upgrade Priorities

This guide aims to assist players in making upgrade choices that will help them be more effective in war, and make faster progress through their town hall levels.

This guide does not have to be followed strictly, however following the upgrade order described below is highly advised.

## Upgrade Order

1. Heroes
2. Storage, Laboratory, Clan Castle
3. Barracks, Army Camps, Spell Factories
4. Resource Collectors
5. Traps
6. Defences
	* Air Defences, Air Sweepers
  * Wizard Towers
  * Mortars
  * Archer Towers
  * Cannons
  * Bomb Towers
7. Walls
8. Xbows, Infernos, Eagle Artillery, Scattershots
9. Town Hall

## Heroes

It takes at least 396 days to get your Archer Queen or Barbarian King from level 1 to 75, so the sooner you start, the better. Heroes are one of the most important upgrades in the game, because the power of your heroes can be one of the biggest deciding factors to determine whether you will be able to 3 star a base.

Heroes should be an ongoing priority. Try to prioritise hero upgrades earlier on so that you don't have to wait around and play a catch-up game when you have completed all your other upgrades. Having your heroes at their max level for your current town hall will also help you farm the rest of the resources that you need to get through the rest of your upgrades.

## Storage

Resource storage are very important upgrades, because the extra space is necessary for certain upgrades, and you will be able to store more loot when all of your builders are busy.

## Laboratory

The Laboratory is a very worthwhile upgrade because you will be able to start upgrading your favourite troops, and any troops that you need to strengthen your army compositions. Ideally, you should aim to have upgrades running in the lab constantly, because the lab is one of the hardest buildings to max in the game, taking a total of 4 years, 73 days and 18 hours to max everything.

Time is the most valuable resource. Due to the significantly longer upgrade times in the lab, it is more efficient to spend your clan war league medals on hammers of fighting or spells as opposed to hammers of heroes or building, as these hammers cost 120 medals (as opposed to 165 for the Hammer of Heroes), and have the ability to instantly complete very long laboratory upgrades, which can only be done one at a time.

## Clan Castle

Upgrading the Clan Castle allows you to bring more powerful reinforcements into battle, and the extra capacity significantly improves your defensive strength.

## Barracks

It’s a great idea to unlock new troops right away, so you can start upgrading them in the Laboratory. You only need to upgrade one barrack to unlock a new troop, as each additional upgrade reduces the training time for that troop, however, while upgrading a barrack, all troop training times will be increased due to the barrack not being available for training. For that reason, it’s a good idea to upgrade barracks over time and not all at once.

## Army Camps

The more troops you can bring into battle, the better. However, it is important to focus on quality over quantity, as a large army of weak troops will do significantly worse than a small army of strong troops. We recommend upgrading your Laboratory and Barracks first, and then your Army Camps, so that you can start getting stronger troops, and then have more of them.

## Spell Factories

Your Spell Factory upgrades are important, but they need considerable planning. While upgrading, you will be unable to brew spells for the duration of the upgrade. It’s a good idea not to upgrade both Spell Factories at the same time; as for example, having only dark elixir spells is better than having no spells.

If possible, try to plan the upgrade around wartime, but if not, please brew your spells before you start the upgrade so that you still have some for your war attacks.

## Resource Collectors

Upgrading your resource collectors helps you gain a greater passive resource income. The more resources you can generate, the more progress you can make.

## Traps

The value of traps is often significantly underrated. They're hidden to enemy attackers, and their potential to deal high amounts of damage can easily disrupt an enemy's attack. By upgrading your traps, you gain a much greater defensive capability for a very minimal increase in war weight.
