module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  darkMode: 'media', // Use prefers-color-scheme
  theme: {
    extend: {
      fontFamily: {
        sans: ['Omnes', 'Verdana', 'Geneva', 'Tahoma', 'sans-serif'],
        eboracum: ['Eboracum', 'sans-serif'],
        esthetique: ['Esthetique', 'sans-serif'],
        semibold: ['Omnes Semibold', 'sans-serif'],
      },
      fontSize: {
        'x-large': '1.5rem',
        large: '1.25rem',
      },
      colors: {
        body: 'var(--color-body)',
        background: 'var(--color-background)',
        'background-layer': 'var(--color-background-layer)',
        text: 'var(--color-text)',
        'text-muted': 'var(--color-text-muted)',
        header: 'var(--color-header)',
        'link': 'var(--color-link)',
        'link-visited': 'var(--color-link-visited)',
      },
      animation: {
        wobble: 'wobble 0.8s ease-in-out infinite',
      },
      keyframes: {
        wobble: {
          '0%, 100%': { transform: 'rotate(-3deg)' },
          '50%': { transform: 'rotate(3deg)' },
        },
      },
    },
    typography: {
      DEFAULT: {
        css: {
          maxWidth: 'none',
          ul: { listStyleType: 'disc', paddingLeft: '1.25rem' },
          ol: { listStyleType: 'decimal', paddingLeft: '1.25rem' },
          p: { fontSize: '1.25rem', lineHeight: '1.75rem' },
          h1: { fontSize: '2.25rem', fontWeight: '700', marginTop: '2rem', marginBottom: '1rem' },
          h2: { fontSize: '1.875rem', fontWeight: '600', marginTop: '1.5rem', marginBottom: '0.75rem' },
          h3: { fontSize: '1.5rem', fontWeight: '500', marginTop: '1rem', marginBottom: '0.5rem' },
          h4: { fontSize: '1.25rem', fontWeight: '500', marginTop: '0.75rem', marginBottom: '0.25rem' },
          a: { textDecoration: 'underline', color: '#3b82f6', '&:hover': { color: '#2563eb' } },
          blockquote: { fontStyle: 'italic', borderLeftWidth: '4px', borderLeftColor: '#d1d5db', paddingLeft: '1rem' },
          pre: { backgroundColor: '#1f2937', color: '#ffffff', padding: '1rem', borderRadius: '0.5rem', overflowX: 'auto' },
          code: { backgroundColor: '#e5e7eb', color: '#dc2626', padding: '0.25rem', borderRadius: '0.25rem' },
        },
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
};
