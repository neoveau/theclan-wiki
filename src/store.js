import { defineStore } from "pinia";
import axios from "axios";

const COC_API_URL = "/api";
const DISCORD_API_URL = "https://discord.com/api/guilds/497709876521205760/widget.json"
const CACHE_DURATION = 5 * 60 * 1000; // 5 minutes in milliseconds

export const useMainStore = defineStore({
  id: "main",

  state: () => ({
    clan: null,
    members: [],
    currentWar: {},
    warLog: [],
    clanWarLeague: {},
    discord: {},
    loading: {
      clan: false,
      members: false,
      currentWar: false,
      warLog: false,
      clanWarLeague: false,
    },
    error: null,
    lastFetchTimestamp: {
      clan: null,
      members: null,
      currentWar: null,
      warLog: null,
      clanWarLeague: null,
    },
  }),

  actions: {
    async fetchData(api, endpoint, dataProp, loadingProp) {
      const now = Date.now();
      const lastFetchTime = this.lastFetchTimestamp[dataProp];

      // Check if the data is already in the store and within the cache duration
      if (lastFetchTime && now - lastFetchTime < CACHE_DURATION) {
        return;
      }

      this.loading[loadingProp] = true;
      try {
        const response = await axios.get(`${api}${endpoint}`);
        this[dataProp] = response.data;
        this.lastFetchTimestamp[dataProp] = now;
      } catch (error) {
        this.error = error;
      } finally {
        this.loading[loadingProp] = false;
      }
    },

    async fetchClan() {
      await this.fetchData(COC_API_URL, "/clan", "clan", "clan");
    },
    async fetchMembers() {
      await this.fetchData(COC_API_URL, "/members", "members", "members");
    },
    async fetchCurrentWar() {
      await this.fetchData(COC_API_URL, "/currentwar", "currentWar", "currentWar");
    },
    async fetchWarLog() {
      await this.fetchData(COC_API_URL, "/warlog", "warLog", "warLog");
    },
    async fetchClanWarLeague() {
      await this.fetchData(COC_API_URL, "/clanwarleague", "clanWarLeague", "clanWarLeague");
    },
    async fetchDiscordServer() {
      await this.fetchData(DISCORD_API_URL, "", "discord", "discord");
    }
  },
});