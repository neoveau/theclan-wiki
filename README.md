# The Clan Wiki

[![Vue.js](https://img.shields.io/badge/vuejs-%2335495e.svg?style=for-the-badge&logo=vuedotjs&logoColor=%234FC08D)](https://vuejs.org)
[![Express.js](https://img.shields.io/badge/express.js-%23404d59.svg?style=for-the-badge&logo=express&logoColor=%2361DAFB)](https://expressjs.com)
[![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)](https://nodejs.org)
[![Render](https://img.shields.io/badge/Render-%46E3B7.svg?style=for-the-badge&logo=render&logoColor=white)](https://render.com)

The Clan Wiki is a project aimed at creating a wiki for the passionate and friendly Clash of Clans community of @ the clan. The project utilizes an Express.js server to fetch data from the Clash of Clans API and displays it on a Vue.js frontend.

## Project Structure
The server side of the project is built using Express.js.
The client side utilizes Vue.js.
Page content is stored in the /public/content directory.
All pages are written in markdown format and imported using markdown-it.

## Setup
To use the project, follow these steps:

1. Clone the repository to your local machine.
2. Create an `.env` file in the root directory of the project.
3. Add the following values to the `.env` file:
```
API_KEY=YourClashOfClansAPIKey
SERVER_PORT=DesiredServerPort
```
Make sure to replace YourClashOfClansAPIKey with your actual Clash of Clans API key, and DesiredServerPort with the port number you want the server to run on.

## Usage
Once the setup is complete, you can start using The Clan Wiki. Run the following commands:

1. Install the project dependencies:
```shell
npm install
```
Start the server:
```shell
npm run build
npm run start
```
Open your browser and visit http://localhost:SERVER_PORT, where SERVER_PORT is the port number you specified in the `.env` file.

## Contributing
Contributions to The Clan Wiki are welcome! If you want to contribute, please follow these steps:

1. Fork the repository.
2. Create a new branch for your feature or bug fix.
3. Make your changes.
4. Commit and push your changes to your forked repository.
5. Submit a pull request to the main repository.

Please ensure that your code follows the project's coding style and conventions.

## License
The Clan Wiki is released under the MIT License. Feel free to use, modify, and distribute the project as per the terms of the license.

## Acknowledgments
- The Clash of Clans API for providing the data used in The Clan Wiki.
- The Express.js and Vue.js communities for their excellent frameworks and libraries.