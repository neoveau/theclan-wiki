const express = require("express");
const { Client, GatewayIntentBits, EmbedBuilder, Partials, REST, Routes, ApplicationCommandOptionType } = require('discord.js');
const axios = require("axios");
const path = require("path");
require("dotenv").config();

// Express app setup
const app = express();
const port = process.env.SERVER_PORT;
const COC_API_URL = "https://api.clashofclans.com/v1";
const CACHE_TTL = 5 * 60 * 1000;
const cache = new Map();

app.use(express.static("dist"));

// Cache middleware
function cacheMiddleware(ttl = CACHE_TTL) {
  return (req, res, next) => {
    const key = req.originalUrl;
    const cachedResponse = cache.get(key);

    if (cachedResponse) {
      const { data, timestamp } = cachedResponse;
      if (Date.now() - timestamp < ttl) {
        return res.json(data);
      } else {
        cache.delete(key);
      }
    }
    next();
  };
}

// Function to handle API requests
function handleAPIRequest(url, req, res) {
  const options = {
    method: "GET",
    url,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${process.env.API_KEY}`,
    },
  };

  axios
    .request(options)
    .then((response) => {
      cache.set(req.originalUrl, {
        data: response.data,
        timestamp: Date.now(),
      });
      res.json(response.data);
    })
    .catch((error) => {
      console.log("Could not fetch " + url);
      res.status(500).json({ error: "An error occurred" });
    });
}

// API endpoints
app.get("/api/cache/clear", (req, res) => {
  cache.clear();
  res.json({ message: "Cache cleared successfully" });
});

app.get("/api/cache/status", (req, res) => {
  const status = {
    size: cache.size,
    keys: Array.from(cache.keys()),
    entries: Array.from(cache.entries()).map(([key, value]) => ({
      key,
      age: Math.round((Date.now() - value.timestamp) / 1000) + ' seconds',
      expires_in: Math.round((CACHE_TTL - (Date.now() - value.timestamp)) / 1000) + ' seconds'
    }))
  };
  res.json(status);
});

app.get("/api/clan", cacheMiddleware(), (req, res) => {
  handleAPIRequest(COC_API_URL + "/clans/%23PVRLQ9YL", req, res);
});

app.get("/api/currentwar", cacheMiddleware(60 * 1000), (req, res) => {
  handleAPIRequest(COC_API_URL + "/clans/%23PVRLQ9YL/currentwar", req, res);
});

app.get("/api/warlog", cacheMiddleware(), (req, res) => {
  handleAPIRequest(COC_API_URL + "/clans/%23PVRLQ9YL/warlog", req, res);
});

app.get("/api/members", cacheMiddleware(), (req, res) => {
  handleAPIRequest(COC_API_URL + "/clans/%23PVRLQ9YL/members", req, res);
});

app.get("/api/clanwarleague", cacheMiddleware(), (req, res) => {
  handleAPIRequest(COC_API_URL + "/clans/%23PVRLQ9YL/currentwar/leaguegroup", req, res);
});

// Discord bot setup
const commands = [
  {
    name: 'wiki',
    description: 'Access clan wiki information',
    options: [
      {
        name: 'clan',
        description: 'Get clan information',
        type: ApplicationCommandOptionType.Subcommand,
      },
      {
        name: 'members',
        description: 'List clan members',
        type: ApplicationCommandOptionType.Subcommand,
        options: [
          {
            name: 'page',
            description: 'Page number to view',
            type: ApplicationCommandOptionType.Integer,
            required: false,
          }
        ]
      },
      {
        name: 'capital',
        description: 'View clan capital information',
        type: ApplicationCommandOptionType.Subcommand,
      },
      {
        name: 'war',
        description: 'Get current war information',
        type: ApplicationCommandOptionType.Subcommand,
      },
      {
        name: 'help',
        description: 'Show available commands',
        type: ApplicationCommandOptionType.Subcommand,
      }
    ]
  }
];

const rest = new REST({ version: '10' }).setToken(process.env.DISCORD_TOKEN);

const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.GuildMembers,
    GatewayIntentBits.DirectMessages,
  ],
  partials: [
    Partials.Channel,
    Partials.Message,
    Partials.User,
    Partials.GuildMember,
  ],
});

// Helper functions
function formatNumber(num) {
  return new Intl.NumberFormat().format(num);
}

function createClanOverviewEmbed(clanData) {
  return new EmbedBuilder()
    .setColor('#0099ff')
    .setTitle(clanData.name)
    .setThumbnail(clanData.badgeUrls.medium)
    .setDescription(clanData.description)
    .addFields(
      { name: 'Level', value: clanData.clanLevel.toString(), inline: true },
      { name: 'Members', value: `${clanData.members}/50`, inline: true },
      { name: 'Location', value: clanData.location?.name || 'Not set', inline: true },
      { name: 'War League', value: clanData.warLeague.name, inline: true },
      { name: 'Capital League', value: clanData.capitalLeague.name, inline: true },
      { name: 'War Stats', value: `Wins: ${clanData.warWins} | Losses: ${clanData.warLosses} | Ties: ${clanData.warTies}`, inline: false },
      { name: 'Points', value: `Clan: ${formatNumber(clanData.clanPoints)} | Capital: ${formatNumber(clanData.clanCapitalPoints)} | Builder Base: ${formatNumber(clanData.clanBuilderBasePoints)}`, inline: false }
    )
    .setFooter({ text: `Clan Tag: ${clanData.tag}` });
}

function createMembersEmbed(memberList, page = 1) {
  const membersPerPage = 10;
  const start = (page - 1) * membersPerPage;
  const end = start + membersPerPage;
  const members = memberList.slice(start, end);

  const embed = new EmbedBuilder()
    .setColor('#0099ff')
    .setTitle('Clan Members')
    .setDescription(`Showing members ${start + 1}-${Math.min(end, memberList.length)} of ${memberList.length}`);

  members.forEach((member) => {
    const roleEmoji = {
      leader: '👑',
      coLeader: '⭐',
      admin: '🛡️',
      member: '👤'
    }[member.role];

    embed.addFields({
      name: `${roleEmoji} ${member.name}`,
      value: `TH${member.townHallLevel} | Trophies: ${member.trophies} | Donations: ${member.donations}/${member.donationsReceived}`,
      inline: false
    });
  });

  return embed;
}

function createCapitalOverviewEmbed(clanData) {
  const districts = clanData.clanCapital.districts;

  return new EmbedBuilder()
    .setColor('#0099ff')
    .setTitle('Clan Capital Overview')
    .addFields(
      { name: 'Capital Hall Level', value: clanData.clanCapital.capitalHallLevel.toString(), inline: true },
      { name: 'Capital League', value: clanData.capitalLeague.name, inline: true },
      { name: 'Capital Points', value: formatNumber(clanData.clanCapitalPoints), inline: true },
      { name: 'Districts', value: districts.map(d => `${d.name}: Level ${d.districtHallLevel}`).join('\n'), inline: false }
    );
}

function createCWLEmbed(cwlData) {
  const embed = new EmbedBuilder()
    .setColor('#0099ff')
    .setTitle('Clan War League Status');

  if (cwlData.state === "preparation") {
    embed.setDescription(`Season: ${cwlData.season}\nState: ${cwlData.state}`);

    // Create a formatted list of participating clans
    const clanList = cwlData.clans.map((clan, index) => {
      return `${index + 1}. ${clan.name} (Level ${clan.clanLevel})`;
    }).join('\n');

    embed.addFields({
      name: 'Participating Clans',
      value: clanList
    });

  } else if (cwlData.state === "inWar") {
    // Get current round's wars
    const currentRound = cwlData.rounds.findIndex(round =>
      round.warTags.some(tag => tag !== "#0")
    ) + 1;

    embed.setDescription(`Season: ${cwlData.season}\nCurrent Round: ${currentRound}/7`);

    // Add current matchups
    const matchups = cwlData.clans.reduce((pairs, clan, index) => {
      if (index % 2 === 0) {
        pairs.push([clan, cwlData.clans[index + 1]]);
      }
      return pairs;
    }, []);

    matchups.forEach((pair, index) => {
      if (pair[1]) { // Check if there's an opponent (for odd numbers of clans)
        embed.addFields({
          name: `War ${index + 1}`,
          value: `${pair[0].name} vs ${pair[1].name}`,
          inline: false
        });
      }
    });
  }

  return embed;
}

// Deploy commands
async function deployCommands() {
  try {
    console.log('Started refreshing application (/) commands.');
    await rest.put(
      Routes.applicationCommands(process.env.CLIENT_ID),
      { body: commands }
    );
    console.log('Successfully reloaded application (/) commands.');
  } catch (error) {
    console.error('Error deploying commands:', error);
  }
}

// Bot events
client.once('ready', () => {
  console.log(`Bot logged in as ${client.user.tag}`);
  deployCommands();
});

client.on('interactionCreate', async interaction => {
  if (!interaction.isChatInputCommand()) return;

  console.log(`Received command: ${interaction.commandName}`);

  if (interaction.commandName === 'wiki') {
    const subcommand = interaction.options.getSubcommand();
    console.log(`Subcommand: ${subcommand}`);

    try {
      switch (subcommand) {
        case 'clan':
          const clanData = await axios.get(`http://localhost:${port}/api/clan`);
          const clanEmbed = createClanOverviewEmbed(clanData.data);
          await interaction.reply({ embeds: [clanEmbed] });
          break;

        case 'members':
          const membersData = await axios.get(`http://localhost:${port}/api/clan`);
          const page = interaction.options.getInteger('page') || 1;
          const membersEmbed = createMembersEmbed(membersData.data.memberList, page);
          await interaction.reply({ embeds: [membersEmbed] });
          break;

        case 'capital':
          const capitalData = await axios.get(`http://localhost:${port}/api/clan`);
          const capitalEmbed = createCapitalOverviewEmbed(capitalData.data);
          await interaction.reply({ embeds: [capitalEmbed] });
          break;

        case 'war':
          try {
            const warData = await axios.get(`http://localhost:${port}/api/currentwar`);

            if (warData.data.state === 'notInWar') {
              // Check if clan is in CWL
              const cwlData = await axios.get(`http://localhost:${port}/api/clanwarleague`);

              if (cwlData.data.state) {
                // Clan is in CWL, show CWL information
                const cwlEmbed = createCWLEmbed(cwlData.data);
                await interaction.reply({ embeds: [cwlEmbed] });
              } else {
                // Clan is truly not in any war
                await interaction.reply('The clan is not currently in a war or CWL.');
              }
              return;
            }

            // Regular war embed (existing code)
            const warEmbed = new EmbedBuilder()
              .setColor('#0099ff')
              .setTitle(`War Against ${warData.data.opponent.name}`)
              .addFields(
                { name: 'Status', value: warData.data.state, inline: true },
                { name: 'Team Size', value: warData.data.teamSize.toString(), inline: true },
                { name: `${warData.data.clan.name}`, value: `⭐ ${warData.data.clan.stars}\n💥 ${warData.data.clan.destructionPercentage.toFixed(2)}%`, inline: true },
                { name: `${warData.data.opponent.name}`, value: `⭐ ${warData.data.opponent.stars}\n💥 ${warData.data.opponent.destructionPercentage.toFixed(2)}%`, inline: true }
              );
            await interaction.reply({ embeds: [warEmbed] });

          } catch (error) {
            console.error('Error fetching war status:', error);
            await interaction.reply({
              content: 'An error occurred while fetching war information.',
              ephemeral: true
            });
          }
          break;

        case 'help':
          const helpEmbed = new EmbedBuilder()
            .setColor('#0099ff')
            .setTitle('Clan Wiki Commands')
            .setDescription('Available commands:')
            .addFields(
              { name: '/wiki clan', value: 'Show clan overview', inline: true },
              { name: '/wiki members [page]', value: 'List clan members (10 per page)', inline: true },
              { name: '/wiki capital', value: 'Show clan capital information', inline: true },
              { name: '/wiki war', value: 'Show current war status', inline: true },
              { name: '/wiki help', value: 'Show this help message', inline: true }
            );
          await interaction.reply({ embeds: [helpEmbed] });
          break;
      }
    } catch (error) {
      console.error('Error details:', error);
      await interaction.reply({
        content: 'An error occurred while processing your request. Please try again later.',
        ephemeral: true
      });
    }
  }
});

// Vue app catch-all route
app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "dist", "index.html"));
});

// Start server and bot
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
  client.login(process.env.DISCORD_TOKEN);
});